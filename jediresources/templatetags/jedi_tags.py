from django import template

register = template.Library()


@register.filter
def jedi_filter(dictionary, key):
    return dictionary[key]