from django.contrib import admin
from jediresources.models import Planet, Jedi, Candidate, Test, Question, Answer, Result

# Register your models here.
admin.site.register(Planet)
admin.site.register(Jedi)
admin.site.register(Candidate)
admin.site.register(Test)
admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(Result)