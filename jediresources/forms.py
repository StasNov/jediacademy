from django import forms
from jediresources.models import Candidate


class EnterForm(forms.ModelForm):
    class Meta:
        model = Candidate
        fields = [
            'name',
            'planet',
            'age',
            'email'
        ]

    def __init__(self, *args, **kwargs):
        super(EnterForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = "Имя"
        self.fields['planet'].label = "Планета"
        self.fields['age'].label = "Возраст"
        self.fields['email'].label = "Электронная почта"
        self.fields['email'].help_text = "Пожалуйста, указывайте реальный адрес"

    # I need in commenting this part, because i use my own email for check application
    # def clean(self):
    #     email = self.cleaned_data['email']
    #     if Candidate.objects.filter(email=email).exists():
    #         raise forms.ValidationError('Юнглинг с данным адресом электронной почты уже сдавал тест на Падавана')