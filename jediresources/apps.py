from django.apps import AppConfig


class JediresourcesConfig(AppConfig):
    name = 'jediresources'
