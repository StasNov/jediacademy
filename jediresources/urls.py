from django.urls import path
from jediresources.views import base_view, test_view, result_view, jedi_view, candidate_result_view, send_email

urlpatterns = [
    path('', base_view, name='base'),
    path('test/<candidate_id>/<test_id>', test_view, name='test'),
    path('result', result_view, name='result'),
    path('jedi/<slug>', jedi_view, name='jedi'),
    path('candidate_result/<jedi_slug>/<result_id>', candidate_result_view, name='candidate_result'),
    path('email', send_email, name='email')
]