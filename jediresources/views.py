from random import choice
import collections
import json
from django.conf import settings
from django.core.mail import send_mail

from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from jediresources.forms import EnterForm
from jediresources.models import Candidate, Jedi, Test, Answer, Result
# Create your views here.


def get_test():
    test_list = Test.objects.values_list('id', flat=True)
    random_test_number = choice(test_list)
    test = Test.objects.get(id=random_test_number)
    return test


def base_view(request):
    form = EnterForm(request.POST or None)
    candidate = Candidate.objects.all()
    jedies = Jedi.objects.all()
    if form.is_valid():
        name = form.cleaned_data['name']
        planet = form.cleaned_data['planet']
        age = form.cleaned_data['age']
        email = form.cleaned_data['email']
        test = get_test()
        new_candidate = Candidate.objects.create(
            name=name,
            planet=planet,
            age=age,
            email=email,
            test=test
        )
        new_candidate.save()
        return HttpResponseRedirect(reverse('test', kwargs={'candidate_id': new_candidate.id, 'test_id': test.id}))
    context = {
        'form': form,
        'jedies': jedies
    }
    return render(request, 'base.html', context)


def test_view(request, candidate_id, test_id):
    test = Test.objects.get(id=test_id)
    questions = test.questions.all()
    total = collections.OrderedDict() # to remember the order of inserting of the elements. If to use python 3.6, standart dict do it by default
    for question in questions:
        total[question.question_text] = Answer.objects.filter(question=question)
    context = {
        'test_id': test_id,
        'total': total,
        'candidate_id': candidate_id
    }
    return render(request, 'test.html', context)


def result_view(request):
    if request.POST:
        test = Test.objects.get(id=request.POST['test_id'])
        candidate = Candidate.objects.get(id=request.POST['candidate_id'])
        questions = test.questions.all()
        count = 0
        max_count = test.questions.count()
        all_answers = {}

        for i in range(0, questions.count()):
            correct_answer = Answer.objects.get(question=questions[i], answer_check=True).answer_text
            if 'customRadio-' + str(i+1) in request.POST:
                all_answers[questions[i].question_text] = request.POST['customRadio-' + str(i+1)]
                if request.POST['customRadio-' + str(i+1)] == correct_answer:
                    count += 1
            else:
                all_answers[questions[i].question_text] = 'no answer'

        json_results = json.dumps(all_answers, ensure_ascii=False)

        new_result = Result.objects.create(
            candidate=candidate,
            test=test,
            result=count,
            answers=json_results
        )
        new_result.save()

        context = {
            'count': count,
            'max_count': max_count
        }
        return render(request, 'result.html', context)


def jedi_view(request, slug):
    jedi = Jedi.objects.get(slug=slug)
    padavans = Candidate.objects.filter(jedi=jedi)
    candidates = Candidate.objects.filter(planet=jedi.planet, jedi=None)
    results = collections.OrderedDict()
    for candidate in candidates:
        try:
            id = Result.objects.get(candidate=candidate).id
            results[reverse('candidate_result', kwargs={'jedi_slug': jedi.slug, 'result_id': id})] = candidate
        except Result.DoesNotExist:
            pass

    context = {
        'jedi': jedi,
        'results': results,
        'padavans': padavans
    }
    return render(request, 'jedi.html', context)


def candidate_result_view(request, jedi_slug, result_id):
    jedi = Jedi.objects.get(slug=jedi_slug)
    padavan_count = Candidate.objects.filter(jedi=jedi).count()
    result = Result.objects.get(id=result_id)
    test = result.test
    questions = test.questions.all()
    count = result.result
    max_count = questions.count()
    total = collections.OrderedDict()
    candidate_answers = json.loads(result.answers)
    for question in questions:
        total[question.question_text] = Answer.objects.filter(question=question)
    context = {
        'total': total,
        'count': count,
        'max_count': max_count,
        'candidate': result.candidate,
        'candidate_answers': candidate_answers,
        'jedi': jedi,
        'padavan_count': padavan_count
    }
    return render(request, 'candidate_result.html', context)


def send_email(request):
    if request.POST:
        candidate_id = request.POST['candidate_id']
        jedi_id = request.POST['jedi_id']
        candidate = Candidate.objects.get(id=candidate_id)
        jedi = Jedi.objects.get(id=jedi_id)
        candidate.jedi = jedi
        candidate.save()
        letter_body = 'Вы достойны похвалы наставника и можете с гордостью носить свою косичку падавана. ' \
                      'Кое-чего вы уже достигли, но впереди ещё очень много ступеней, на которые предстоит подняться, ' \
                      'чтобы стать настоящим рыцарем-джедаем.'
        if send_mail('Зачисление в Падаваны', letter_body, settings.EMAIL_HOST_USER, [candidate.email]):
            message_delivery = "Сообщение доставлено"
        else:
            message_delivery = "Сообщение не доставлено"

        context = {
            'message_delivery': message_delivery,
            'jedi': jedi
        }

        return render(request, 'email.html', context)

