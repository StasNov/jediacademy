from django.db import models
from jsonfield import JSONField
from django.urls import reverse


# Function to save a picture to a specific folder with a readable name
def image_folder(instance, filename):
    filename = instance.slug + '.' + filename.split('.')[1]
    return "{0}/{1}".format(instance.slug, filename)

# Create your models here.


class Planet(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Jedi(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField()
    planet = models.ForeignKey(Planet, on_delete=models.PROTECT)
    image = models.ImageField(upload_to=image_folder) # need to install "pillow" for it

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('jedi', kwargs={'slug': self.slug})


class Question(models.Model):
    question_text = models.TextField()

    def __str__(self):
        return self.question_text


class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer_text = models.TextField()
    answer_check = models.BooleanField(default=False)

    def __str__(self):
        return "{0} - вариант ответа на вопрос - {1}".format(self.answer_text, self.question.question_text)


class Test(models.Model):
    title = models.CharField(max_length=100)
    questions = models.ManyToManyField(Question, blank=True)

    def __str__(self):
        return self.title


class Candidate(models.Model):
    name = models.CharField(max_length=200)
    planet = models.ForeignKey(Planet, on_delete=models.PROTECT)
    age = models.PositiveIntegerField()
    email = models.EmailField()
    jedi = models.ForeignKey(Jedi, on_delete=models.PROTECT, null=True)
    test = models.ForeignKey(Test, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        return "{0} from {1} - {2} years old".format(self.name, self.planet.name, self.age)


class Result(models.Model):
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    test = models.ForeignKey(Test, on_delete=models.PROTECT)
    result = models.PositiveIntegerField()
    answers = JSONField()

    def __str__(self):
        return "Результат теста - {0} кандидата - {1}".format(self.test_id, self.candidate_id)



